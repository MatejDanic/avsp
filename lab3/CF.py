from decimal import Decimal, ROUND_HALF_UP
import fileinput
from heapq import nlargest
import numpy as np

line_index = 0
user_item = []
item_avg = []
user_avg = []


def get_adv_val(param):
    I, J, T, K = param
    I -= 1
    J -= 1
    # print(I, J)
    sims = {}
    if T == 0:
        for row_num, row in enumerate(item_avg):
            #print("row -> ", row_num, row)
            if not row == item_avg[I]:
                numer = 0
                denom_1, denom_2 = 0, 0
                for i in range(len(row)):
                    numer += item_avg[I][i] * row[i]
                    denom_1 += item_avg[I][i] * item_avg[I][i]
                    denom_2 += row[i] * row[i]
                # print(numer, denom_1, denom_2)
                sim = numer / (np.sqrt(denom_1) * np.sqrt(denom_2))
                if sim >= 0 and not user_item[row_num][J] == 'X':
                    sims[row_num] = sim
        #print(sims)
        rating = 0
        sum_of_sims = 0
        for n in nlargest(K, sims, key=sims.get):
            rating += int(user_item[n][J]) * sims[n]
            sum_of_sims += sims[n]
        rating /= sum_of_sims
    else:
        for col_num, col in enumerate(user_avg):
            #print("row -> ", row_num, row)
            if not col == user_avg[J]:
                numer = 0
                denom_1, denom_2 = 0, 0
                for i in range(len(col)):
                    numer += user_avg[J][i] * col[i]
                    denom_1 += user_avg[J][i] * user_avg[J][i]
                    denom_2 += col[i] * col[i]
                # print(numer, denom_1, denom_2)
                sim = numer / (np.sqrt(denom_1) * np.sqrt(denom_2))
                if sim >= 0 and not user_item[I][col_num] == 'X':
                    sims[col_num] = sim
        #print(sims)
        rating = 0
        sum_of_sims = 0
        for n in nlargest(K, sims, key=sims.get):
            rating += int(user_item[I][n]) * sims[n]
            sum_of_sims += sims[n]
        rating /= sum_of_sims
    print(Decimal(Decimal(rating).quantize(Decimal('.001'), rounding=ROUND_HALF_UP)))


for line in fileinput.input():
    if line_index == 0:
        N = int(line.split(" ")[0])
        M = int(line.split(" ")[1])
    elif line_index < M + 1:
        user_item.append(line.rstrip().split(" "))
        row = line.rstrip().split(" ")
        row[:] = [0 if x == 'X' else int(x) for x in row]
        avg = sum(row) / np.count_nonzero(row)
        for i in range(len(row)):
            if not row[i] == 0:
                row[i] = float(row[i] - avg)
        item_avg.append(row)
    elif line_index == M + 1:
        item_user = np.transpose(user_item)
        for row in np.transpose(user_item):
            row = row.tolist()
            row[:] = [0 if x == 'X' else int(x) for x in row]
            avg = sum(row) / np.count_nonzero(row)
            for i in range(len(row)):
                if not row[i] == 0:
                    row[i] = float(row[i] - avg)
            user_avg.append(row)
        Q = int(line)
        counter = 0
    elif M + 1 < line_index < M + Q + 2:
        get_adv_val(list(map(int, line.rstrip().split(" "))))
        counter += 1
    line_index += 1

# print(item_avg)
