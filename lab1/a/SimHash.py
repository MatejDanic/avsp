import hashlib as hl
import fileinput


def simhash(text_line):
    sh = [0] * 128
    for unit in text_line.split():
        result = hl.md5(unit.encode())
        result = bin(int(result.hexdigest(), 16))[2:].zfill(128)
        for j in range(len(result)):
            if result[j] == '1':
                sh[j] += 1
            else:
                sh[j] -= 1
    for j in range(len(sh)):
        if sh[j] >= 0:
            sh[j] = 1
        else:
            sh[j] = 0
    return hex(int(''.join(str(x) for x in sh), 2))[2:]


def hamming_distance(hash1, hash2):
    dist = bin(int(hash1, 16) ^ int(hash2, 16))
    return dist.count('1')


if __name__ == "__main__":
    line_index = 0
    hashes = []
    queries = []
    distances = {}
    q = -1
    for line in fileinput.input():
        if line_index == 0:
            n = int(line)
        elif line_index < n + 1:
            hashes.append(simhash(line))
        elif line_index == n + 1:
            q = int(line)
        elif n + 1 < line_index < n + q + 2:
            index, similar_bits = int(line.split()[0]), int(line.split()[1])
            chosen_hash = hashes[index]
            counter = 0
            for i in range(len(hashes)):
                if i != index:
                    if not distances.keys().__contains__(str(index) + " " + str(i))\
                            or not distances.keys().__contains__(str(i) + " " + str(index)):
                        distance = hamming_distance(chosen_hash, hashes[i])
                        distances[str(index) + " " + str(i)] = distance
                    else:
                        distance = distances[str(index) + " " + str(i)]
                    if distance <= similar_bits:
                        counter += 1
            print(counter)
        line_index += 1

