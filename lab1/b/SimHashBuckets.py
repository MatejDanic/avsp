import hashlib as hl
import fileinput


def simhash(text_line):
    sh = [0] * 128
    for unit in text_line.split():
        result = hl.md5(unit.encode())
        result = bin(int(result.hexdigest(), 16))[2:].zfill(128)
        for j in range(len(result)):
            if result[j] == '1':
                sh[j] += 1
            else:
                sh[j] -= 1
    for j in range(len(sh)):
        if sh[j] >= 0:
            sh[j] = 1
        else:
            sh[j] = 0
    return hex(int(''.join(str(x) for x in sh), 2))[2:]


def hamming_distance(hash1, hash2):
    dist = bin(int(hash1, 16) ^ int(hash2, 16))
    # print(bin(int(hash1, 16)))
    # print(bin(int(hash2, 16)))
    # print(dist.zfill(128))
    # print(dist.count('1'))
    return dist.count('1')


def hash_to_int(belt_num, hashed_text):
    bin_num = bin(int(str(bin(int(hashed_text, 16))[2:].zfill(128)), 2))
    # print(bin_num)
    # print(belt_num, bin(int(bin_num[-16 * belt_num:], 2)))
    if belt_num == 1:
        bin_num = (bin(int(bin_num[-16 * belt_num:], 2)), belt_num)
    elif belt_num == 8:
        bin_num = (bin(int(bin_num[-len(bin_num):-16 * (belt_num - 1)], 2)), belt_num)
    else:
        bin_num = (bin(int(bin_num[-16 * belt_num:-16 * (belt_num - 1)], 2)), belt_num)

    return bin_num


if __name__ == "__main__":
    line_index = 0
    hashes = []
    queries = []
    q = -1
    candidates = {}
    distances = {}
    partitions = {}
    b = 8
    for line in fileinput.input():
        if line_index == 0:
            n = int(line)

        elif line_index < n + 1:
            curr_id = line_index - 1
            candidates[str(curr_id)] = []
            hashes.append(simhash(line))
            for belt in range(1, b + 1):
                chosen_hash = hashes[curr_id]
                val = hash_to_int(belt, chosen_hash)
                texts_in_partition = []
                if partitions.keys().__contains__(str(val)):
                    texts_in_partition = partitions[str(val)]
                    for tip in texts_in_partition:
                        if not candidates[str(curr_id)].__contains__(tip):
                            candidates[str(curr_id)].append(tip)
                            candidates[str(tip)].append(curr_id)
                else:
                    texts_in_partition = []
                texts_in_partition.append(curr_id)
                partitions[str(val)] = texts_in_partition

        elif line_index == n + 1:
            q = int(line)

        elif n + 1 < line_index < n + q + 2:
            index, similar_bits = int(line.split()[0]), int(line.split()[1])
            chosen_hash = hashes[index]
            counter = 0
            # print(candidates)
            for candidate in candidates[str(index)]:
                if candidate != index:
                    if not distances.keys().__contains__(str(index) + " " + str(candidate)) \
                            or not distances.keys().__contains__(str(candidate) + " " + str(index)):
                        distance = hamming_distance(chosen_hash, hashes[candidate])
                        distances[str(index) + " " + str(candidate)] = distance
                    else:
                        distance = distances[str(index) + " " + str(candidate)]
                    if distance <= similar_bits:
                        counter += 1
            print(counter)

        line_index += 1
