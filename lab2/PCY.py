import fileinput
from itertools import combinations

line_index = 0
item_count = {}
basket_list = []
for line in fileinput.input():
    if line_index == 0:
        N = int(line)
    elif line_index == 1:
        s = float(line)
        threshold = int(s * N)
    elif line_index == 2:
        b = int(line)
    else:
        item_list = list(map(int, line.split(" ")))
        basket_list.append(item_list)
        for item in item_list:
            if not item_count.keys().__contains__(item):
                item_count[item] = 1
            else:
                item_count[item] += 1
    line_index += 1
comp_dict = {}
k_list = {}
pair_count = {}
for basket in basket_list:
    for c in combinations(basket, 2):
        if item_count[c[0]] >= threshold and item_count[c[1]] >= threshold:
            if c in k_list:
                k = k_list[c]
                pair_count[c] += 1
            else:
                k = (c[0] * len(item_count) + c[1]) % b
                k_list[c] = k
                pair_count[c] = 1
            if k in comp_dict:
                comp_dict[k] += 1
            else:
                comp_dict[k] = 1
pair_dict = {}
for c in k_list:
    if comp_dict[k_list[c]] >= threshold:
        pair_dict[c] = pair_count[c]
m = sum(i > threshold for i in item_count.values())
print(int((m * (m - 1)) / 2))
print(len(pair_dict))
for c in sorted(pair_dict.items(), key=lambda x: x[1], reverse=True):
    print(c[1])
