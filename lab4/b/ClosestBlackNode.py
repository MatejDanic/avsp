import fileinput
#import time


def get_closest_bn(source, d_nodes, counter):
    queue = set()
    #print(source, d_nodes)
    counter += 1
    if counter == 10:
        return -1, -1
    if nodes[source] == 1:
        return source, 0
    for n in d_nodes:
        if nodes[n] == 1:
            closest[n] = n, counter
            return n, counter
        queue = queue.union(dest_nodes[n])
    return get_closest_bn(source, sorted(queue), counter)

#start = time.time()
line_index = 0
for line in fileinput.input():
    if line_index == 0:
        N = int(line.split(" ")[0])
        e = int(line.split(" ")[1])
        nodes = dict.fromkeys(range(N))
        dest_nodes = {}
        closest = dict.fromkeys(range(N))
    elif line_index < N + 1:
        nodes[line_index-1] = int(line.rstrip())
    else:
        v = list(map(int, line.rstrip("").split(" ")))
        if v[0] not in dest_nodes:
            dest_nodes[v[0]] = set()
        if v[1] not in dest_nodes:
            dest_nodes[v[1]] = set()
        dest_nodes[v[0]].add(v[1])
        dest_nodes[v[1]].add(v[0])

        #print(v[0], dest_nodes[v[0]])
        #vertices[v[0], v[1]] = 1
    line_index += 1
for node in nodes:
    pair = get_closest_bn(node, sorted(dest_nodes[node]), 0)
    print(pair[0], pair[1])
# print(nodes)
#print(time.time()-start)
