import fileinput


def calc_ranks(t_max, nodes):
    for i in range(t_max):
        ranks.update(dict.fromkeys(((key, i+1) for key in nodes), constant))
        for key, value in nodes.items():
            prev = beta * ranks[key, i] / len(value)
            for j in value:
                ranks[j, i+1] += prev
        converged = True
        for key in nodes:
            if not format(ranks[key, i+1], '.10f') == format(ranks[key, i], '.10f'):
                converged = False
                break
        if converged and i > 2:
            return i
    return t_max


line_index = 0
t_max = 0
for line in fileinput.input():
    if line_index == 0:
        N = int(line.split(" ")[0])
        beta = float(line.split(" ")[1])
        c = 1 / N
        constant = (1 - beta) / N
        key_set = set([i for i in range(N)])
        dest_nodes = dict.fromkeys(key_set)
        ranks = dict.fromkeys(((key, 0) for key in key_set), c)
    elif line_index < N + 1:
        dest_nodes[line_index - 1] = list(map(int, line.rstrip("").split(" ")))
    elif line_index == N + 1:
        t_conv = calc_ranks(100, dest_nodes)
        Q = int(line)
    else:
        n, t = int(line.rstrip("").split(" ")[0]), int(line.rstrip("").split(" ")[1])
        if t > t_conv:
            t = t_conv
        print(format(ranks[n, t], '.10f'))
    line_index += 1
