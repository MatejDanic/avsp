import fileinput

line_index = 0
t_max = 0
reading_vertices = True
vertices = {}
nodes = {}
for line in fileinput.input():
    if len(line.split(" ")) > 1:
        if reading_vertices:
            pair = line.rstrip().split(" ")
            vertices[pair[0], pair[1]] = 1
            # if int(pair[0]) < int(pair[1]):
            #     vertices[pair[0], pair[1]] = 1
            # else:
            #     vertices[pair[1], pair[0]] = 1

        else:
            features = line.rstrip().split(" ")
            nodes[features[0]] = [features[i] for i in range(1, len(features))]
    else:
        reading_vertices = False
    line_index += 1
for v in vertices:
    similarity = 0
    max_similarity = 0

    for i in range(len(nodes[v[0]])):
        max_similarity += 1
        if nodes[v[0]][i] == nodes[v[1]][i]:
            similarity += 1
    vertices[v] = max_similarity - (similarity - 1)


def get_all_paths(curr, end, visited, taken_path):
    global paths
    visited[curr] = True
    if curr == end:
        paths.append(taken_path[:])
    else:
        for vert in vertices:
            if curr in vert:
                if curr == vert[0]:
                    nxt = vert[1]
                else:
                    nxt = vert[0]
                if not visited[nxt]:
                    taken_path.append(vert)
                    get_all_paths(nxt, end, visited, taken_path)
    visited[curr] = False
    if len(taken_path) > 0:
        taken_path.pop()


def get_path_weight(p):
    path_sum = 0
    for vert in p:
        path_sum += vertices[vert]
    return path_sum


def get_shortest_paths(start, end):
    global paths
    shortest_path_vertices = []
    paths = []
    visited = dict.fromkeys(nodes, False)
    get_all_paths(start, end, visited, [])
    path_weights = {}
    for counter, p in enumerate(paths):
        path_weights[counter] = get_path_weight(p)
    if len(path_weights) == 0:
        return []
    shortest_path_weight = min(path_weights.values())
    for counter, p in enumerate(paths):
        if path_weights[counter] == shortest_path_weight:
            shortest_path_vertices.append(p)
    return shortest_path_vertices


def format_sprut_output(mod_vertices):
    string = ""
    set_list = get_communities(mod_vertices)
    sorted_set_list = []
    for item in set_list:
        sorted_set_list.append(sorted(item, key=lambda element: int(element)))
    sorted_set_list = sorted(sorted_set_list, key=lambda element: (int(element[0])))

    sorted_set_list = sorted(sorted_set_list, key=len)
    for item in sorted_set_list:
        for node in item:
            string += node + "-"
        string = string[:-1] + " "
    return string


paths = []
max_modularity = 0


def get_communities(vert_list):
    set_list = []
    for vert in vert_list:
        found = False
        for item in set_list:
            if vert[0] in item:
                found = True
                item.add(vert[1])
            elif vert[1] in item:
                found = True
                item.add(vert[0])
        if not found:
            new_set = set()
            new_set.add(vert[0])
            new_set.add(vert[1])
            set_list.append(new_set)
    old_len = 0
    curr_len = len(set_list)
    while curr_len != old_len:
        old_len = curr_len
        for node in nodes:
            node_set_index = []
            counter = 0
            remove_queue = []
            for i in range(len(set_list)):
                if node in set_list[i]:
                    counter += 1
                    node_set_index.append(i)
                    if counter > 1:
                        counter = 1
                        set_list[node_set_index[0]] = set_list[node_set_index[0]] | set_list[i]
                        remove_queue.append(i)
            for i in remove_queue:
                if i < len(set_list):
                    set_list.pop(i)
        curr_len = len(set_list)

    for node in nodes:
        found = False
        for item in set_list:
            if node in item:
                found = True
        if not found:
            new_set = set()
            new_set.add(node)
            set_list.append(new_set)
    return set_list


def get_modularity(vert_list):
    set_list = get_communities(vert_list)
    # print(set_list)
    # print(vert_list)
    graph = {}

    q = 0
    for node_i in range(0, len(nodes)):
        for node_j in range(0, len(nodes)):
            u, w = list(nodes)[node_i], list(nodes)[node_j]
            k_u, k_w = 0, 0
            for vert in vert_list:
                if u in vert:
                    k_u += vertices[vert]
                if w in vert:
                    k_w += vertices[vert]

            # print(u, w)
            same_community = False
            for item in set_list:
                if u in item and w in item:
                    same_community = True
            if same_community:
                if u == w:
                    q += 0
                elif (u, w) in vertices:
                    # print("u, w", u, w)
                    q += vertices[(u, w)]
                elif (w, u) in vertices:
                    # print("w, u", w, u)
                    q += vertices[(w, u)]
                # print("(", k_u, "*", k_w, ") /", 2*sum(vert_list.values()), "=",
                #       (k_u * k_w) / (2*sum(vertices.values())))
                q -= (k_u * k_w) / (2*sum(vertices.values()))
    q /= (2*sum(vertices.values()))
    q = round(q, 4)
    # print(q)
    return q


max_mod_vertices = vertices
while vertices:
    centrality = {}
    for i in range(0, len(nodes)):
        for j in range(i + 1, len(nodes)):
            shortest_paths = get_shortest_paths(list(nodes)[i], list(nodes)[j])
            for path in shortest_paths:
                for v in path:
                    if v not in centrality:
                        centrality[v] = 0
                    centrality[v] += 1 / len(shortest_paths)
    modularity = get_modularity(vertices)
    if modularity > max_modularity:
        max_modularity = modularity
        max_mod_vertices = vertices.copy()
    pop_keys = []
    max_cent = max(centrality.values())
    for cent in centrality:
        if centrality[cent] == max_cent:
            if int(cent[0]) < int(cent[1]):
                k = (cent[0], cent[1])
            else:
                k = (cent[1], cent[0])
            pop_keys.append(k)
    pop_keys = sorted(pop_keys, key=lambda element: (int(element[0]), int(element[1])))
    for k in pop_keys:
        # print(k)
        print(k[0], k[1])
        # print(centrality[k])
        if (k[0], k[1]) in centrality:
            centrality.pop(k)
            vertices.pop(k)
        else:
            centrality.pop((k[1], k[0]))
            vertices.pop((k[1], k[0]))

print(format_sprut_output(max_mod_vertices))
